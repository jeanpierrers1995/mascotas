
# __Author__: Jeanpierre Rivas Soncco. #

#  Reto Divelopers Mascotas - Proceso para Correr el proyecto (Python) Django ##
Este proyecto se desarrolló con python 3.11 
### Para correr el proyecto se debe tener instalado python 3.11 y crear su entorno virtual con dicha versión de python, luego activar su entorno virtual y después correr los siguientes comandos ###
    
    pip install -r requirements
    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver

### Usuario:
    - Para este proyecto se creo un usuario y contraseña:
        - usuario: jeanpierre
        - contraseña: 1234


### APIS GENERADAS
    - Para este proyecto se uso el paquete "swagger" para generar la documentación de las apis.
    - Para acceder a la documentación de las apis se debe ingresar a la siguiente url:
        - http://localhost:8000/swagger/

### BD
    - Para este proyecto se uso la misma DB que nos proporciona DJANGO, es decir, la DB SQLITE3.
    - En el archivo .env del settings django cambiar la configuración de la base de datos.
    - en el archivo .env existe una VARIABLE llamada "DATABASE_URL" que contiene la configuración de la base de datos.

### Ejemplo para asociar los juguetes a una mascota:

    - Existe un endpoint para asociar los juguetes a una mascota, para ello se debe enviar un json con el id de la mascota y el id de los juguetes que se desean asociar.
        - http://localhost:8000/api/pets/mascotas/asociar-juguetes/<str:nombre_corto>/
    - Primera debes tener tus juguetes creados.
    - Segunda debes tener tu mascota creada.
    - Debes asignarle los juguetes a la mascota, en este caso 3 juguetes por mascota.
        - http://localhost:8000/api/pets/mascotas/asociar-juguetes/princesa/
    - Te vas a Raw Data:

        {
            "id": 3,
            "juguetes": [],
            "nombre": "Princesa",
            "imagen": "http://localhost:8000/media/mascotas/digital_ocean.png",
            "nombre_corto": "princesa"
        }
    - En "juguetes" pones los juguetes que han sido creados en juguetes, si no te votará alguna exception de validation que no existe el juegute.
    - Se aplican diferentes validacion como las pidió.
    - Por ejemplo así:
        {
            "id": 3,
            "juguetes": ["juguete1", "juguete2", "juguete3"],
            "nombre": "Princesa",
            "imagen": "http://localhost:8000/media/mascotas/digital_ocean.png",
            "nombre_corto": "princesa"
        }
    - Enviar con el método PUT y listo.
    - El resultado quería así:
        HTTP 200 OK
        Allow: GET, PUT, PATCH, HEAD, OPTIONS
        Content-Type: application/json
        Vary: Accept
        
        {
            "id": 3,
            "juguetes": [
                {
                    "id": 2,
                    "nombre": "juguete1",
                    "precio": "120.00",
                    "url_compra": "https://www.facebook.com"
                },
                {
                    "id": 3,
                    "nombre": "juguete2",
                    "precio": "12.00",
                    "url_compra": "https://www.facebook.com"
                },
                {
                    "id": 4,
                    "nombre": "juguete3",
                    "precio": "12.00",
                    "url_compra": "https://www.facebook.com"
                }
            ],
            "nombre": "Princesa",
            "imagen": "http://localhost:8000/media/mascotas/digital_ocean.png",
            "nombre_corto": "princesa"
        }
    - En lo demás es más facil de realizarlo.
