from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Mascota, Juguete, Usuario


class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		extra_kwargs = {'password': {'write_only': True}}
	
	def create(self, validated_data):
		user = User.objects.create_user(**validated_data)
		return user


class UsuarioSerializer(serializers.ModelSerializer):
	user = UserSerializer()
	
	class Meta:
		model = Usuario
		fields = '__all__'
	
	def create(self, validated_data):
		user_data = validated_data.pop('user')
		user = UserSerializer().create(user_data)
		usuario = Usuario.objects.create(user=user, **validated_data)
		return usuario


class JugueteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Juguete
		fields = '__all__'


class MascotaSerializer(serializers.ModelSerializer):
	usuario = serializers.HiddenField(default=serializers.CurrentUserDefault())
	juguetes = JugueteSerializer(many=True, required=False)
	
	# nombre_corto = serializers.CharField(read_only=True)  # Campo de solo lectura
	
	class Meta:
		model = Mascota
		fields = '__all__'
