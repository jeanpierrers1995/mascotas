from django.contrib import admin

from apps.pets.models import Usuario, Mascota, Juguete

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Mascota)
admin.site.register(Juguete)



