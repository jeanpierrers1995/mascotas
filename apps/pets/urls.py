from django.urls import path
from .views import MascotaListCreateView, MascotaRetrieveUpdateDeleteView, JugueteListCreateView, \
    JugueteRetrieveUpdateDeleteView, UsuarioCreateView, UsuarioRetrieveUpdateDeleteView, AsociarJuguetesMascotaView

urlpatterns = [
    path('usuarios/', UsuarioCreateView.as_view(), name='usuario-create'),
    path('usuarios/<int:pk>/', UsuarioRetrieveUpdateDeleteView.as_view(), name='usuario-retrieve-update-delete'),
    
    path('mascotas/', MascotaListCreateView.as_view(), name='mascota-list-create'),
    path('mascotas/<int:pk>/', MascotaRetrieveUpdateDeleteView.as_view(), name='mascota-retrieve-update-delete'),
    
    path('juguetes/', JugueteListCreateView.as_view(), name='juguete-list-create'),
    path('juguetes/<int:pk>/', JugueteRetrieveUpdateDeleteView.as_view(), name='juguete-retrieve-update-delete'),
    
    path('mascotas/asociar-juguetes/<str:nombre_corto>/', AsociarJuguetesMascotaView.as_view(),
         name='asociar-juguetes-mascota'),

]
