from django.contrib.auth.models import User
from rest_framework.response import Response

from .models import Mascota, Juguete, Usuario
from .serializers import MascotaSerializer, JugueteSerializer, UsuarioSerializer
from rest_framework.exceptions import ValidationError
from rest_framework import generics, authentication, permissions, status


class UsuarioCreateView(generics.CreateAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer


class UsuarioRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer


class MascotaListCreateView(generics.ListCreateAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer
    authentication_classes = [authentication.SessionAuthentication, authentication.BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    
    def perform_create(self, serializer):
        nombre_corto = serializer.validated_data.get('nombre_corto')
        if not nombre_corto.isalnum():
            raise ValidationError("El nombre corto solo debe contener caracteres alfanuméricos.")
        if Mascota.objects.filter(usuario=self.request.user, nombre_corto=nombre_corto).exists():
            raise ValidationError("Ya existe una mascota con este nombre corto para este usuario.")
        serializer.save()


class MascotaRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer


class JugueteListCreateView(generics.ListCreateAPIView):
    queryset = Juguete.objects.all()
    serializer_class = JugueteSerializer


class JugueteRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Juguete.objects.all()
    serializer_class = JugueteSerializer


class AsociarJuguetesMascotaView(generics.RetrieveUpdateAPIView):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer
    lookup_field = 'nombre_corto'
    authentication_classes = [authentication.SessionAuthentication, authentication.BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def update(self, request, *args, **kwargs):
        mascota = self.get_object()
        juguetes = request.data.get('juguetes', [])

        if len(juguetes) > 3:
            return Response({'error': 'Una mascota no puede tener más de 3 juguetes.'}, status=status.HTTP_400_BAD_REQUEST)

        mascota.juguetes.clear()  # Eliminar cualquier asociación previa de juguetes
        for juguete_nombre in juguetes:
            try:
                juguete = Juguete.objects.get(nombre=juguete_nombre)
                mascota.juguetes.add(juguete)
            except Juguete.DoesNotExist:
                pass

        return Response(self.get_serializer(mascota).data)
