from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from rest_framework.exceptions import ValidationError

AREAS_TRABAJO = (
	('Desarrollo', 'Desarrollo'),
	('Diseño', 'Diseño'),
	('Ventas', 'Ventas'),
)


class Usuario(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	email = models.EmailField()
	area_trabajo = models.CharField(max_length=20, choices=AREAS_TRABAJO)
	
	def __str__(self):
		return f"{self.nombre} {self.apellido} - {self.area_trabajo}"


class Mascota(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	imagen = models.ImageField(upload_to='mascotas/')
	nombre_corto = models.CharField(max_length=50, unique=True, db_index=True, validators=[RegexValidator(r'^[a-zA-Z0-9]+$', 'El nombre corto solo debe contener caracteres alfanuméricos.')])
	
	usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_mascota')
	
	juguetes = models.ManyToManyField('Juguete', blank=True, related_name='mascotas_mascotas')
	
	def save(self, *args, **kwargs):
		if self.pk is not None:  # Si ya existe, no permitir modificar el nombre corto
			old_mascota = Mascota.objects.get(pk=self.pk)
			if old_mascota.nombre_corto != self.nombre_corto:
				raise ValidationError({"nombre_corto": "El nombre corto no puede ser modificado."})
		super().save(*args, **kwargs)
	
	def __str__(self):
		return self.nombre_corto


class Juguete(models.Model):
	nombre = models.CharField(max_length=100)
	precio = models.DecimalField(max_digits=8, decimal_places=2)
	url_compra = models.URLField()
	
	def __str__(self):
		return self.nombre
