# Generated by Django 3.2.16 on 2023-07-26 08:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pets', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mascota',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, unique=True)),
                ('imagen', models.ImageField(upload_to='mascotas/')),
                ('nombre_corto', models.CharField(db_index=True, max_length=50, unique=True)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mascotas', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('apellido', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=254)),
                ('area_trabajo', models.CharField(choices=[('Desarrollo', 'Desarrollo'), ('Diseño', 'Diseño'), ('Ventas', 'Ventas')], max_length=20)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameModel(
            old_name='Regalo',
            new_name='Juguete',
        ),
        migrations.RemoveField(
            model_name='mascotas',
            name='user',
        ),
        migrations.DeleteModel(
            name='MascotaRegalo',
        ),
        migrations.DeleteModel(
            name='Mascotas',
        ),
        migrations.DeleteModel(
            name='UsuarioMascota',
        ),
    ]
