from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Mascotas, Regalo, MascotaRegalo


class AgregarRegaloPorMascotaView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pet_short_name):
        try:
            pet = Mascotas.objects.get(short_name=pet_short_name, user=request.user)
        except Mascotas.DoesNotExist:
            return Response({"error": "La mascota no existe o no pertenece al usuario actual."},
                            status=status.HTTP_404_NOT_FOUND)

        if request.method == 'POST':
            toy_id = request.data.get('toy_id')
            try:
                toy = Regalo.objects.get(id=toy_id)
            except Regalo.DoesNotExist:
                return Response({"error": "El juguete con el ID proporcionado no existe."},
                                status=status.HTTP_404_NOT_FOUND)

            if MascotaRegalo.objects.filter(pet=pet).count() >= 3:
                return Response({"error": "La mascota ya tiene asignados 3 juguetes. No se pueden agregar más."},
                                status=status.HTTP_400_BAD_REQUEST)

            # Asociar el juguete con la mascota
            MascotaRegalo.objects.create(pet=pet, toy=toy)

            return Response({"message": "Juguete agregado exitosamente a la mascota."}, status=status.HTTP_201_CREATED)